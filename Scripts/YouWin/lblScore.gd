extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	self.set_text("You Scored: " + str(get_node("/root/Global").total_score))

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
