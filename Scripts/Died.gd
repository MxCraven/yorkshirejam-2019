extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	self.hide()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Body_body_entered(body):
	if body.name == "Player":
		self.show()


func _on_btnRestart_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()
