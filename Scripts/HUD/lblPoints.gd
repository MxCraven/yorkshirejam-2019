extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var score = 0

func _ready():
	score = get_node("/root/Global").total_score
	self.set_text("Score: " + str(score))

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_CoinArea_body_entered(body):
	if body.name == "Player":
		score += 100
		self.set_text("Score: " + str(score))
	


func _on_Enemy_enemy_killed():
	score += 100
	self.set_text("Score: " + str(score))
