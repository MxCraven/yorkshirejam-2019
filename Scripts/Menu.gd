extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	#Reset the score
	get_node("/root/Global").total_score = 0

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_btnPlay_pressed():
	get_tree().change_scene("res://Scenes/Dales1.tscn")


func _on_btnQuit_pressed():
	get_tree().quit()
