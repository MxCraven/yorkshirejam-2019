extends KinematicBody2D

export var distance = 2
export var speed = 100
export var direction = "right"

signal enemy_killed

var timer = Timer.new()

func _ready():
	set_process(true)
	
	timer.connect("timeout", self, "run_timer")
	
	add_child(timer)
	run_timer()
	

func _process(delta):
	if direction == "left":
		move_and_slide(Vector2(-speed, 0))
	if direction == "right":
		move_and_slide(Vector2(speed, 0))
	
	

func run_timer():
	
	if direction == "left":
		direction = "right"
	else:
		direction = "left"
	
	timer.set_wait_time(distance)
	
		
	timer.start()
	



func _on_Head_body_entered(body):
	if body.name == "Player":
		emit_signal("enemy_killed")
		self.queue_free()
